<?php

namespace CodeEduStore\Repositories;

use CodeEduStore\Models\ProductStore;
use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository
 * @package namespace CodePub\Repositories;
 */
interface OrderRepository extends RepositoryInterface, RepositoryCriteriaInterface
{
    public function process($token, $user, ProductStore $productStore);
}
