<section class="jumbotron banner">
    <div class="col-md-12 text-center">
        <h1>Escreva um livro</h1>
        <p>Construa o seu lugar</p>
        <hr>
        <p><span>Transforme suas ideias em livros profissionais.</span></p>
        <a href="{{route('books.index')}}" class="btn btn-success">Começar</a>
    </div>
</section>