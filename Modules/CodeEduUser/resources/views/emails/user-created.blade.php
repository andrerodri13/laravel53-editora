<h3>{{config('app.name')}}</h3>
<p>Sua conta na plataforma foi criada</p>
<p>Usuário: <b>{{$user->email}}</b></p>
<p>
    <?php $link = route('codeeduuser.email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email); ?>
    Clique aqui para verificar sua conta <a href="{{$link}}" target="_blank">{{$link}}</a>
</p>

<p>Obs.: Não responda este e-mail, ele é gerado automaticamente</p>
