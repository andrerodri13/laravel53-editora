@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Listagem de papéis de usuário</h3>
            {!! Button::primary('Novo papel de usuário')->asLinkTo(route('codeeduuser.roles.create')) !!}
        </div>
        <br/>
        <div class="row">
            {!! Form::model(compact('search'), [ 'class' => 'form-inline', 'method' => 'GET']) !!}
            {!! Form::label('search', 'Perquisar por Nome', ['class' => 'control-label']) !!}
            {!! Form::text('search', null, ['class' => 'form-control']) !!}

            {!! Button::primary('Buscar')->submit() !!}
            {!! Form::close() !!}
        </div>

        <div class="row">
            {!!
            Table::withContents($roles->items())->striped()
            ->callback('Ações', function($field, $role){
                $linkEdit = route('codeeduuser.roles.edit', ['role' => $role->id]);
                $linkDestroy = route('codeeduuser.roles.destroy', ['role' => $role->id]);
                $linkEditPermission = route('codeeduuser.roles.permission.edit', ['role' => $role->id]);
                $deleteForm =  "deleteForm-{$role->id}";
                $form =  Form::open(['route' => ['codeeduuser.roles.destroy', 'role' => $role->id],

                                    'method' => 'DELETE', 'id'=> $deleteForm, 'style' => 'display:none'])
                                    . Form::close();
                 $anchorDestroy = Button::link('Excluir')->asLinkTo($linkDestroy)->addAttributes([
                                  'onclick' => "event.preventDefault(); document.getElementById(\"{$deleteForm}\").submit();"
                                  ]);
                $anchorFlag = '<a title="Não é possível excluír o papel Admin e Autor">Excluir</a>';
                $anchorDestroy = ($role->name == config('codeeduuser.acl.role_admin') || $role->name == config('codeedubook.acl.role_author')) ? $anchorFlag : $anchorDestroy;
                return "<ul class=\"list-inline\">".
                        "<li>".Button::link('Editar')->asLinkTo($linkEdit)."</li>".
                        "<li>|</li>".
                        "<li>".$anchorDestroy."</li>".
                        "<li>|</li>".
                        "<li>".Button::link('Permissões')->asLinkTo($linkEditPermission)."</li>".

                       "</ul>" . $form;
            })
            !!}
            {{$roles->links()}}
        </div>
    </div>
@endsection