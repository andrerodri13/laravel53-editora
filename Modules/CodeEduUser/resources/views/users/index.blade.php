@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Listagem de Usuários</h3>
            {!! Button::primary('Novo Usuário')->asLinkTo(route('codeeduuser.users.create')) !!}
        </div>
        <br/>
        <div class="row">
            {!! Form::model(compact('search'), [ 'class' => 'form-inline', 'method' => 'GET']) !!}
            {!! Form::label('search', 'Perquisar por Nome', ['class' => 'control-label']) !!}
            {!! Form::text('search', null, ['class' => 'form-control']) !!}

            {!! Button::primary('Buscar')->submit() !!}
            {!! Form::close() !!}
        </div>

        <div class="row">
            {!!
            Table::withContents($users->items())->striped()
            ->callback('Ações', function($field, $user){
                $linkEdit = route('codeeduuser.users.edit', ['user' => $user->id]);
                $linkDestroy = route('codeeduuser.users.destroy', ['user' => $user->id]);
                $deleteForm =  "deleteForm-{$user->id}";
                $form =  Form::open(['route' => ['codeeduuser.users.destroy', 'user' => $user->id],

                                    'method' => 'DELETE', 'id'=> $deleteForm, 'style' => 'display:none'])
                                    . Form::close();
                $anchorDestroy = Button::link('Excluir')->asLinkTo($linkDestroy)->addAttributes([
                                  'onclick' => "event.preventDefault(); document.getElementById(\"{$deleteForm}\").submit();"
                                  ]);
                if($user->id == \Auth::user()->id){
                    $anchorDestroy->disable();
                }
                return "<ul class=\"list-inline\">".
                        "<li>".Button::link('Editar')->asLinkTo($linkEdit)."</li>".
                        "<li>|</li>".
                        "<li>".$anchorDestroy."</li>".
                       "</ul>" . $form;
            })
            !!}
            {{$users->links()}}
        </div>
    </div>
@endsection