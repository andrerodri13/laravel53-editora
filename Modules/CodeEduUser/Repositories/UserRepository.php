<?php

namespace CodeEduUser\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BookRepository
 * @package namespace CodePub\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
