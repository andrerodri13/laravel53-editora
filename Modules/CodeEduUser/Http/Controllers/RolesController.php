<?php

namespace CodeEduUser\Http\Controllers;

use CodeEduUser\Criteria\FindPermissionGroupCriteria;
use CodeEduUser\Criteria\FindPermissionResourcesCriteria;
use CodeEduUser\Facade\PermissionReader;
use CodeEduUser\Http\Requests\PermissionRequest;
use CodeEduUser\Http\Requests\RoleDeleteRequest;
use CodeEduUser\Http\Requests\RoleRequest;
use CodeEduUser\Http\Requests\UserDeleteRequest;
use CodeEduUser\Http\Requests\UserRequest;
use CodeEduUser\Repositories\PermissionRepository;
use CodeEduUser\Repositories\RoleRepository;
use Illuminate\Database\QueryException;
use CodeEduUser\Annotations\Mapping as Permission;


/**
 * @Permission\Controller(name = "role-admin", description="Administração de papéis de usuário")
 */
class RolesController extends Controller
{

    /**
     * @var RoleRepository
     */
    private $repository;
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    public function __construct(RoleRepository $repository, PermissionRepository $permissionRepository)
    {
        $this->repository = $repository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     * @Permission\Action(name="list", description="Listar papéis de usuário")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->repository->paginate(10);
        return view('codeeduuser::roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     * @Permission\Action(name="store", description="Cadastrar papel de usuário")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('codeeduuser::roles.create');
    }


    /**
     * @Permission\Action(name="store", description="Cadastrar papel de usuário")
     * @param RoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleRequest $request)
    {
        $this->repository->create($request->all());
        $url = $request->get('redirect_to', route('codeeduuser.users.index'));
        $request->session()->flash('message', 'Papel Cadastrado com sucesso.');

        return redirect()->to($url);
    }


    /**
     * Show the form for editing the specified resource.
     * @Permission\Action(name="update", description="Atualizar papel de usuário")
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) //Route Model Binding
    {
        $role = $this->repository->find($id);
        return view('codeeduuser::roles.edit', compact('role'));
    }


    /**
     * @Permission\Action(name="update", description="Atualizar papel de usuário")
     * @param RoleRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoleRequest $request, $id)
    {
        $data = $request->except('permissions');
        $this->repository->update($data, $id);
        $url = $request->get('redirect_to', route('codeeduuser.roles.index'));
        $request->session()->flash('message', 'Papel Alterado com sucesso.');

        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @Permission\Action(name="destroy", description="Excluír papel de usuário")
     * @param RoleDeleteRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoleDeleteRequest $request, $id)
    {
        try {
            $this->repository->delete($id);
            \Session::flash('message', 'Papel Deletado com sucesso.');

        } catch (QueryException $ex) {
            \Session::flash('error', 'Papel de usuário não pode ser excluído. Ele está relacionado com outros registros.');

        }
        return redirect()->to(\URL::previous());
    }

    public function editPermission($id)
    {
        PermissionReader::getPermissions();
        $role = $this->repository->find($id);

        $this->permissionRepository->pushCriteria(new FindPermissionResourcesCriteria());
        $permissions = $this->permissionRepository->all();

        $this->permissionRepository->resetCriteria();
        $this->permissionRepository->pushCriteria(new FindPermissionGroupCriteria());
        $permissionsGroup = $this->permissionRepository->all(['name', 'description']);

        return view('codeeduuser::roles.permissions', compact('role', 'permissions', 'permissionsGroup'));
    }

    public function updatePermission(PermissionRequest $request, $id)
    {
        $data = $request->get('permissions', []);
        $this->repository->updatePermissions($data, $id);
        $url = $request->get('redirect_to', route('codeeduuser.roles.index'));
        $request->session()->flash('message', 'Permissões atribuídas com sucesso. .');

        return redirect()->to($url);
    }

}
