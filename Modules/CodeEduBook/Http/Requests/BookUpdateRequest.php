<?php

namespace CodeEduBook\Http\Requests;

use CodeEduBook\Http\Requests\BookCreateRequest;
use CodeEduBook\Repositories\BookRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class BookUpdateRequest extends BookCreateRequest
{

}
