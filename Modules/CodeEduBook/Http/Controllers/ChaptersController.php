<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Criteria\FindByAuthor;
use CodeEduBook\Criteria\FindByBook;
use CodeEduBook\Criteria\OrderByOrder;
use CodeEduBook\Http\Requests\ChapterCreateRequest;
use CodeEduBook\Http\Requests\ChapterUpdateRequest;
use CodeEduBook\Models\Book;
use CodeEduBook\Repositories\BookRepository;
use CodeEduBook\Repositories\ChapterRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use CodeEduUser\Annotations\Mapping as Permission;

/**
 * @Permission\Controller(name = "book-admin", description="Administração de livros")
 */
class ChaptersController extends Controller
{
    /**
     * @var ChapterRepository
     */
    private $repository;
    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * ChaptersController constructor.
     * @param ChapterRepository $repository
     * @param BookRepository $bookRepository
     */
    public function __construct(ChapterRepository $repository, BookRepository $bookRepository)
    {
        $this->repository = $repository;
        $this->bookRepository = $bookRepository;
        $this->bookRepository->pushCriteria(new  FindByAuthor());
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Display a listing of the resource.
     * @param Request $request
     * @param Book $book
     * @return Response
     * @internal param $id
     */
    public function index(Request $request, Book $book)
    {
        $search = $request->get('search');
        $this->repository->pushCriteria(new FindByBook($book->id))->pushCriteria(new OrderByOrder());
        $chapters = $this->repository->paginate(10);
        return view('codeedubook::chapters.index', compact('chapters', 'search', 'book'));
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Show the form for creating a new resource.
     * @param Book $book
     * @return Response
     * @internal param $id
     */
    public function create(Book $book)
    {
        return view('codeedubook::chapters.create', compact('book'));
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Store a newly created resource in storage.
     * @param ChapterCreateRequest|Request $request
     * @param Book $book
     * @return Response
     * @internal param $id
     */
    public function store(ChapterCreateRequest $request, Book $book)
    {
        $data = $request->all();
        $data['book_id'] = $book->id;
        $this->repository->create($data);
        $url = $request->get('redirect_to', route('chapters.index', ['book' => $book->id]));
        $request->session()->flash('message', 'Capítulo cadastrado com sucesso.');
        return redirect()->to($url);
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Show the form for editing the specified resource.
     * @param Book $book
     * @param $chapterId
     * @return Response
     */
    public function edit(Book $book, $chapterId)
    {
        $this->repository->pushCriteria(new FindByBook($book->id));
        $chapter = $this->repository->find($chapterId);
        return view('codeedubook::chapters.edit', compact('chapter', 'book'));
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Update the specified resource in storage.
     * @param ChapterUpdateRequest|Request $request
     * @param Book $book
     * @param $chapterId
     * @return Response
     */
    public function update(ChapterUpdateRequest $request, Book $book, $chapterId)
    {
        $this->repository->pushCriteria(new FindByBook($book->id));
        $data = $request->except(['book_id']);
        $this->repository->update($data, $chapterId);
        $url = $request->get('redirect_to', route('chapters.index', ['book' => $book->id]));
        $request->session()->flash('message', 'Capítulo alterado com sucesso');
        return redirect()->to($url);
    }

    /**
     * @Permission\Action(name="chapter", description="Capítulos")
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Book $book, $chapterId)
    {
        $this->repository->pushCriteria(new FindByBook($book->id));
        $this->repository->delete($chapterId);
        \Session::flash('message', 'Capítulo excluido com sucesso');
        return redirect()->to(\URL::previous());
    }
}
