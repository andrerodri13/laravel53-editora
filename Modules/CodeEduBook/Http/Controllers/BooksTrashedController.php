<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Criteria\FindByAuthor;
use CodeEduBook\Repositories\BookRepository;

use Illuminate\Http\Request;
use CodeEduUser\Annotations\Mapping as Permission;

/**
 * @Permission\Controller(name = "book-trashed-admin", description="Administração de livros excluídos")
 */
class BooksTrashedController extends Controller
{
    /**
     * @var BookRepository
     */
    private $repository;
    /**
     * @var \CodeEduBook\Repositories\CategoryRepository
     */
    private $categoryRepository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
        $this->repository->pushCriteria(new FindByAuthor());
    }

    /**
     * Display a listing of the resource.
     * @Permission\Action(name="list", description="Listar livros Excluídos")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $books = $this->repository->onlyTrashed()->with('author')->paginate(10);
        return view('codeedubook::trashed.books.index', compact('books', 'search'));
    }

    /**
     * Display a listing of the resource.
     * @Permission\Action(name="list", description="Listar livros Excluídos")
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->repository->onlyTrashed();
        $book = $this->repository->find($id);

        return view('codeedubook::trashed.books.show', compact('book'));
    }

    /**
     * @Permission\Action(name="restore", description="Restaurar livro da lixeira")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->repository->onlyTrashed();
        $this->repository->restore($id);
        $url = $request->get('redirect_to', route('trashed.books.index'));
        $request->session()->flash('message', 'Livro Restaurado com sucesso.');
        return redirect()->to($url);
    }
}