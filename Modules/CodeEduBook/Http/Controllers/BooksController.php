<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Criteria\FindByAuthor;
use CodeEduBook\Http\Requests\BookCoverRequest;
use CodeEduBook\Http\Requests\BookCreateRequest;
use CodeEduBook\Http\Requests\BookUpdateRequest;
use CodeEduBook\Jobs\GenerateBook;
use CodeEduBook\Models\Book;
use CodeEduBook\Notifications\BookExported;
use CodeEduBook\Pub\BookCoverUpload;
use CodeEduBook\Repositories\BookRepository;
use CodeEduBook\Repositories\CategoryRepository;
use CodePub;
use Illuminate\Http\Request;
use CodeEduUser\Annotations\Mapping as Permission;

/**
 * @Permission\Controller(name = "book-admin", description="Administração de livros")
 */
class BooksController extends Controller
{
    /**
     * @var BookRepository
     */
    private $repository;
    /**
     * @var \CodeEduBook\Repositories\CategoryRepository
     */
    private $categoryRepository;

    public function __construct(BookRepository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->repository->pushCriteria(new FindByAuthor());
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     * @Permission\Action(name="list", description="Listar livros")
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $books = $this->repository->paginate(10);
        return view('codeedubook::books.index', compact('books', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     * @Permission\Action(name="store", description="Cadastrar livro")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $users = User::query()->pluck('name', 'id');
        $categories = $this->categoryRepository->lists('name', 'id'); //pluck na trait BaseRepository
        return view('codeedubook::books.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @Permission\Action(name="store", description="Cadastrar livro")
     * @param BookCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookCreateRequest $request)
    {
        $data = $request->all();
        $data['author_id'] = \Auth::user()->id;
        $this->repository->create($data);
        $url = $request->get('redirect_to', route('books.index'));
        $request->session()->flash('message', 'Livro Cadastrado com sucesso.');
        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     * @Permission\Action(name="update", description="Atualizar livro")
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Book $book)
    {
        $this->categoryRepository->withTrashed();
        $categories = $this->categoryRepository->listsWithMutators('name_trashed', 'id'); //pluck na trait BaseRepository
        return view('codeedubook::books.edit', compact('book', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @Permission\Action(name="update", description="Atualizar livro")
     * @param BookUpdateRequest $request
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(BookUpdateRequest $request, Book $book)
    {
        $data = $request->except(['author_id']);
        $data['published'] = $request->get('published', false);
        $this->repository->update($data, $book->id);
        $url = $request->get('redirect_to', route('books.index'));
        $request->session()->flash('message', 'Livro Alterado com sucesso.');
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @Permission\Action(name="destroy", description="Excluír livro")
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Book $book)
    {
        $this->repository->delete($book->id);
        \Session::flash('message', 'Livro excluído com sucesso');
        return redirect()->to(\URL::previous());
    }

    /**
     * @Permission\Action(name="cover", description="Cover de livro")
     * @param Book $book
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function coverForm(Book $book)
    {
        return view('codeedubook::books.cover', compact('book'));
    }

    /**
     * @Permission\Action(name="cover", description="Cover de livro")
     * @param BookCoverRequest $request
     * @param Book $book
     * @param BookCoverUpload $upload
     * @return \Illuminate\Http\RedirectResponse
     */
    public function coverStore(BookCoverRequest $request, Book $book, BookCoverUpload $upload)
    {
        $upload->upload($book, $request->file('file'));
        $url = $request->get('redirect_to', route('books.index'));
        $request->session()->flash('message', 'Cover adicionado com sucesso.');
        return redirect()->to($url);
    }


    /**
     * @Permission\Action(name="export", description="Exportar livro")
     * @param Book $book
     * @return \Illuminate\Http\RedirectResponse
     */
    public function export(Book $book)
    {
        dispatch(new GenerateBook($book));
        \Auth::user()->notify(new BookExported(\Auth::user(), $book));
        return redirect()->route('books.index');
    }


    /**
     * @Permission\Action(name="download", description="Download do livro")
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Book $book)
    {
        return response()->download($book->zip_file);
    }

    public function downloadCommon($id)
    {
        $book = $this->repository->find($id);
        if (\Gate::allows('book-download', $book->id)) {
            return $this->download($book);
        }
        abort(404);
    }
}