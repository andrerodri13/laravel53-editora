<?php

namespace CodeEduBook\Pub;

use CodeEduBook\Models\Book;
use Illuminate\Http\UploadedFile;
use Imagine\Image\Box;

class BookCoverUpload
{
    /**
     * @param Book $book
     * @param UploadedFile $cover
     * @internal param UploadedFile $file
     */
    public function upload(Book $book, UploadedFile $cover)
    {
        \Storage::disk(config('codeedubook.book_storage'))
            ->putFileAs($book->ebook_template, $cover, $book->cover_ebook_name);

        $this->makeCoverPdf($book);
        $this->makeThumnail($book);
    }

    protected function makeCoverPdf(Book $book)
    {
        if (!is_dir($book->pdf_template_storage)) {
            mkdir($book->pdf_template_storage, 0755, true);
        }

        $img = new \Imagick($book->cover_ebook_file);
        $img->setImageFormat('pdf');

        $img->writeImage($book->cover_pdf_file);
    }

    protected function makeThumnail(Book $book)
    {
        if (!is_dir($book->thumbs_storage)) {
            mkdir($book->thumbs_storage, 0755, true);
        }

        $coverEbookFile = $book->cover_ebook_file;
        $thumnail = \Image::open($coverEbookFile)->thumbnail(new Box(356, 522));
        $thumnail->save($book->thumbnail_file);

        $thumnailSmall = \Image::open($coverEbookFile)->thumbnail(new Box(138, 230));
        $thumnailSmall->save($book->thumbnail_small_file);
    }
}