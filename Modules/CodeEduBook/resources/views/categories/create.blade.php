@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h3>Criar categorias</h3>
            {{--@if($errors->any())--}}
            {{--<ul class="alert alert-danger list-inline">--}}
            {{--@foreach($errors->all() as $error)--}}
            {{--<li>{{$error}}</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
            {{--@endif--}}
            {{--{!! Alert::success('Olá Mundo') !!}--}}
            {!! Form::open(['route' => 'categories.store', 'class' => 'form']) !!}

            @include('codeedubook::categories._form')


            {!! Html::openFormGroup() !!}
            {!! Button::primary('Criar Categoria')->submit() !!}
            {!! Html::closeFormGroup() !!}

            {!! Form::close() !!}
        </div>
    </div>



@endsection