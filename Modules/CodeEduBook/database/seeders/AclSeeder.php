<?php

use Illuminate\Database\Seeder;

class AclSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAuthor = \CodeEduUser\Models\Role::where('name', config('codeedubook.acl.role_author'))->first();
        $permissionBook = \CodeEduUser\Models\Permission::where('name', 'like', 'book%')->pluck('id')->all();
        $permissionCategory = \CodeEduUser\Models\Permission::where('name', 'like', 'category%')->pluck('id')->all();

        $roleAuthor->permissions()->attach($permissionBook);
        $roleAuthor->permissions()->attach($permissionCategory);
    }
}
