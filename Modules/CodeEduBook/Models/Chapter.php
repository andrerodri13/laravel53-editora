<?php

namespace CodeEduBook\Models;

use Bootstrapper\Interfaces\TableInterface;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model implements TableInterface
{
    protected $fillable = [
        'name',
        'content',
        'order',
        'book_id'
    ];


    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function getTableHeaders()
    {
        return ['#', 'Nome', 'Order'];
    }

    public function getValueForHeader($header)
    {
        switch ($header) {
            case '#':
                return $this->id;
            case 'Nome':
                return $this->name;
            case 'Order':
                return $this->order;
        }

    }
}
