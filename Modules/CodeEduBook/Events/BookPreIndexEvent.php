<?php

namespace CodeEduBook\Events;

use CodeEduBook\Models\Book;
use Illuminate\Queue\SerializesModels;

class BookPreIndexEvent
{
    /**
     * @var Book
     */
    private $book;
    private $rankin = 0;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Book $book)
    {
        //
        $this->book = $book;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @return int
     */
    public function getRankin()
    {
        return $this->rankin;
    }

    /**
     * @param int $rankin
     * @return BookPreIndexEvent
     */
    public function setRankin($rankin)
    {
        $this->rankin = $rankin;
        return $this;
    }


}
